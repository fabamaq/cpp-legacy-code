# Working Effectively with Legacy Code

Some guidelines from the book "Working Effectively with Legacy Code" from Michael C. Feathers.

<center><b>Take them with a grain of salt</b></center>

---

> Code without tests is bad code. It doesn't matter how well written it is; it doesn't matter how pretty or object-oriented or well-encapsulated it is. With tests, we can change the behavior of our code quickly and verifiably. WIthout them, we really don't know if our code is getting better or worse. &mdash; Michael C. Feathers.

---
---
## Part I: The Mechanics of Change

---

### Chapter 1: Changing Software

> For simplicity's sake, let's look at four primary reasons to change software.
1. Adding a feature
2. Fixing a bug
3. Improving the design
4. Optimizing resource usage

> In general, three different things can change when we do work in a system: structure, functionality, and resource usage.

|                   | Adding a Feature | Fixing a Bug | Refactoring | Optimizing |
|        :-:        |       :--:       |     :--:     |     :-:     |    :--:    |
|     Structure     |     Changes      |    Changes   |   Changes   |   &mdash;  |
| New Functionality |     Changes      |    &mdash;   |   &mdash;   |   &mdash;  |
| Fix Functionality |     &mdash;      |    Changes   |   &mdash;   |   &mdash;  |
|  Resource Usage   |     &mdash;      |    &mdash;   |   &mdash;   |   Changes  |

---

### Chapter 2: Working with Feedback

> Changes in a system can be made in two primary ways:
* **Edit and Pray**
* **Cover and Modify**
> Covering software means covering it with tests.

#### What Is Unit Testing?

> A Unit Test that takes 1/10th of a second to run is a slow unit test

#### Higher-Level Testing


#### Test Coverings

> Dependency is one of the most critical problems in software development. Much legacy code work involves **breaking dependencies** so that change can be easier.

> **The Legacy Code Dilema**
> > When we change code, we should have tests in place. To put tests in place, we often have to change code.

#### The Legacy Code Change Algorithm

1. Identify change points
2. Find test points.
3. Break dependencies.
4. Write tests.
5. Make changes and refactor.


---

### Chapter 3: Sensing and Separation

1. **Sensing** - We break dependencies to _sense_ when we can't access values our code computes.
2. **Separation** - We break dependencies to _separate_ when we can't even get a piece of code into a test harness to run.

#### Faking Collaborators

> **Fake Objects**
> * A _fake object_ is an object that impersonates some collaborator of your class when it is being tested.

---

### Chapter 4: The Seam Model

> **Seam**
> * A seam is a place where you can alter behavior in your program without editing in that place.

> **Seam Types**
> * **Preprocessing Seams**
> * * In C and C++, a macro preprocessor runs before the compiler. Preprocessing seams are pretty powerful; they can be used to replace/redirect calls from production code to fake code.
> * **Link Seams**
> * * In C and C++, the linker combines the compiler-generated object code into a complete code. When a source file contains an `#include` statement, the compiler checks to see if the included component is defined. It necessary, it defers to the linker to find its definition at link time. The enabling point is the library that contains the definition.
> * **Object Seams**
> * * Object seams are pretty much the most useful seams available in object-oriented programming languages.


---

### Chapter 5: Tools

#### Automated Refactoring Tools

> **refactoring** (n.). A change made to the internal structure of software to make it easier to understand and cheaper to modify without changing its exsiting behavior.

There are a lot of tools to refactor code (i.e. IDEs and plugins like Resharper).

#### Mock Objects

> One of the big problems that we confront in legacy code work is dependency. If we want to execute a piece of code by itself (e.g. Test) and see what it does, often we have to break dependencies on other code. To do so, we use Mock Objects, which are usually provided in Mock Frameworks (e.g. googlemock).

---
---
## Part II: Changing Software

---

### Chapter 6: I Don’t Have Much Time and I Have to Change It

* **Sprout Method**
* **Sprout Class**
* **Wrap Method**
* **Wrap Class**

##### * See Ch06 code examples

---

### Chapter 7: It Takes Forever to Make a Change

#### Understanding

> As the amount of code in a project grows, it gradually surpasses understanding.

> However, there is one key difference between a well-maintained system and a legacy system. In a well-maintained system, it might take a while to figure out how to make a change, but once you do, the change is usually easy and you feel much more comfortable with the system. In a legacy system, it can take a long time to figure out what to do, and the change is difficult also.

#### Lag Time

> Lag time is the amount of time that passes between a change that you make and the moment that you get real feedback about the change.

> You should be able to compile every class or module in your system separately from the others and in its own test harness.

#### Break Dependencies

> In object-oriented code, often the first step is to attempt to instantiate the classes that we need in a test harness. (...) When you are able to create an object of a class in a test harness, you might have other dependencies to break if you want to test individual methods.

> Usually, the execution cost for most methods is relatively low compared to the costs of the methods that they call, particularly if the calls are calls to external resources such as the database, hardware, or the communications infrstructure.

#### Build Dependencies

> In an object-oriented system, if you have a cluster of classes that you want to build more quickly, the first thing that you have to figure out is which dependencies will get in the way.

> The way to hadle this is to extract interfaces for the classes in your cluster that are used by classes outside the cluster.

```c++
#include "AddOpportunityXMLGenerator.hpp"
#include "OpportunityItem.hpp"
#include "ConsultantSchedulerDB.hpp" // creates OpportunityItem

class AddOpportunityFormHandler {
    public:
        AddOpportunityFormHandler(ConsultantSchedulerDB& aConsultantSchedulerDB)
        : mConsultantSchedulerDB(aConsultantSchedulerDB)
        {}

    private:
        ConsultantSchedulerDB& mConsultantSchedulerDB;
}
```

> The first dependency we encounter is `ConsultantSchedulerDB`. We need to create one to pass to the `AddOpportunityFormHandler` constructor. It would be awkward to use that class because it connects to the database, and we don't want to do that during testing. However, we could use _Extract Implementer_ and break the dependency:

```c++
/// ----------------------------------------
/// @file ConsultantSchedulerDB.hpp
/// ----------------------------------------
#include "OpportunityItem.hpp"

class ConsultantSchedulerDB {
    public:
        virtual OpportunityItem* CreateOpportunityItem() = 0;
}

/// ----------------------------------------
/// @file ConsultantSchedulerDBImpl.hpp
/// ----------------------------------------
#include "ConsultantSchedulerDB.hpp"
class ConsultantSchedulerDBImpl : public ConsultantSchedulerDB {
    public:
        OpportunityItem* CreateOpportunityItem() override;
}

/// ----------------------------------------
/// @file AddOpportunityFormHandler.hpp
/// ----------------------------------------
#include "AddOpportunityXMLGenerator.hpp"
#include "OpportunityItem.hpp"
#include "ConsultantSchedulerDB.hpp"

class AddOpportunityFormHandler {
    public:
        AddOpportunityFormHandler(ConsultantSchedulerDB& aConsultantSchedulerDB)
        : mConsultantSchedulerDB(aConsultantSchedulerDB)
        {}

    private:
        ConsultantSchedulerDB& mConsultantSchedulerDB;
}
```

We can go even further by using _Extract Implementer_ on the `OpportunityItem` class.

```c++
/// ----------------------------------------
/// @file OpportunityItem.hpp
/// ----------------------------------------
class OpportunityItem {};

/// ----------------------------------------
/// @file OpportunityItemImpl.hpp
/// ----------------------------------------
#include "OpportunityItem.hpp"

class OpportunityItemImpl : public OpportunityItem {};

/// ----------------------------------------
/// @file ConsultantSchedulerDB.hpp
/// ----------------------------------------
#include "OpportunityItem.hpp"

class ConsultantSchedulerDB {
    public:
        virtual OpportunityItem* CreateOpportunityItem() = 0;
}

/// ----------------------------------------
/// @file ConsultantSchedulerDBImpl.hpp
/// ----------------------------------------
#include "ConsultantSchedulerDB.hpp"
#include "OpportunityItemImpl.hpp"

class ConsultantSchedulerDBImpl : public ConsultantSchedulerDB {
    public:
        /// @note using Covariant Return Type
        OpportunityItemImpl* CreateOpportunityItem() override;
}

/// ----------------------------------------
/// @file AddOpportunityFormHandler.hpp
/// ----------------------------------------
#include "AddOpportunityXMLGenerator.hpp"
#include "OpportunityItem.hpp"
#include "ConsultantSchedulerDB.hpp"

class AddOpportunityFormHandler {
    public:
        AddOpportunityFormHandler(ConsultantSchedulerDB& aConsultantSchedulerDB)
        : mConsultantSchedulerDB(aConsultantSchedulerDB)
        {}

    private:
        ConsultantSchedulerDB& mConsultantSchedulerDB;
}
```

> If we wanted to make this "compilation firewall" explicit in the package structure of the application, we could break up our design into separate packages (libraries):
> * **OpportunityProcessing**
> * + AddOpportunityFormHandler
> * - AddOpportunityXMLGenerator
> * **DatabaseGateway**
> * + ConsultantSchedulerDB
> * + OpportunityItem
> * **DatabaseImplementation**
> * + ConsultantSchedulerDBImpl
> * + OpportunityItemImpl

> This uses the **Dependency Inversion Principle**.

> The final structure is:
> * **OpportunityProcessing**
> * + AddOpportunityFormHandler
> * + AddOpportunityFormHandlerTest
> * - AddOpportunityXMLGenerator
> * - AddOpportunityXMLGeneratorTest
> * **DatabaseGateway**
> * + ConsultantSchedulerDB
> * + OpportunityItem
> * **DatabaseImplementation**
> * + ConsultantSchedulerDBImpl
> * + ConsultantSchedulerDBImplTest
> * + OpportunityItemImpl
> * + OpportunityItemImplTest

---

### Chapter 8: How Do I Add a Feature?

#### Test-Driven Development

1. Write a failing test case.
2. Get it to compile.
3. Make it pass.
4. Remove duplication.
5. Repeat.

##### * See Ch08/TestDrivenDevelopment code examples

#### Programming by Difference

> We can use inheritance to introduce features without modifying a class directly. After we've added the feature, we can figure out exactly how we really want the feature integrated. The key technique for doing this is something called _programming by difference_.

##### * See Ch08/ProgrammingByDifference code examples

> After a bit of refactoring, the structure is:
> * **MessageForwarder**
> * +MessageForwarder()
> * +processMessage(Message)
> * -forwardMessage(Message)
> * **MailingConfiguration / MailingList**
> * +getFromAddress(Message)
> * +buildRecipientList(List recipients) : List

##### * Watch out for Liskov Substitution Principle violations

---

### Chapter 9: I Can’t Get This Class into a Test Harness

> Here are the four most common problems we encounter:
> 1. Objects of the class can't be created easily.
> 2. The test harness won't easily build with the class in it.
> 3. The constructor we need to use has bad side effects.
> 4. Significant work happens in the constructor, and we need to sense it.

Examples

* **The Case of the Irritating Parameter**

##### * See Ch09 code examples

---

### Chapter 10: I Can’t Run This Method in a Test Harness

...

---

### Chapter 11: I Need to Make a Change. What Methods Should I Test?

---

### Chapter 12: I Need to Make Many Changes in One Area. Do I Have to Break Dependencies for All the Classes Involved?

---

### Chapter 13: I Need to Make a Change, but I Don’t Know What Tests to Write

---

### Chapter 14: Dependencies on Libraries Are Killing Me

---

### Chapter 15: My Application Is All API Calls

---

### Chapter 16: I Don’t Understand the Code Well Enough to Change It

---

### Chapter 17: My Application Has No Structure

---

### Chapter 18: My Test Code Is in the Way

---

### Chapter 19: My Project Is Not Object Oriented. How Do I Make Safe Changes?

---

### Chapter 20: This Class Is Too Big and I Don’t Want It to Get Any Bigger

---

### Chapter 21: I’m Changing the Same Code All Over the Place

---

### Chapter 22: I Need to Change a Monster Method and I Can’t Write Tests for It

---

### Chapter 23: How Do I Know That I’m Not Breaking Anything?

---

### Chapter 24: We Feel Overwhelmed. It Isn’t Going to Get Any Better

---
---
## Part III: Dependency-Breaking Techniques

---

### Chapter 25: Dependency-Breaking Techniques

---

## Appendix: Refactoring