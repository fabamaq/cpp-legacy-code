/**
 * @brief Wrap Method:
 * 1. Identify a method you need to change.
 * 2. If the change can be formulated as
 * ... a single sequence of statements in one place,
 * ... rename the method and then create a new method
 * ... with the same name and signature as the old method.
 * ... // ! Remember to Preserve Signatures as you do this.
 * 3. Place a call to the old method in the new method.
 * 4. Develop a method for the new feature, test first (TDD),
 * ... and call it from the new method.
 */
#include "main.hpp"

void Employee::pay() { dispatchPayment(); }
// void Employee::makeLoggedPayment() {
//   logPayment();
//   dispatchPayment();
// }

/**
 * @brief We are adding up daily timecards for an employee and then
 * sending his payment information to a PayDispatcher.
 */
void Employee::dispatchPayment() {
	Money amount = Money();
	// for (Iterator it = timecards.iterator(); it.hasNext();)
	for (auto it = timecards.begin(); it != timecards.end(); ++it) {
		Timecard card = *it;
		if (payPeriod.contains(date)) {
			amount.add(card.getHours() * payRate);
		}
	}
	payDispatcher.pay(this, date, amount);
}

void Employee::logPayment() {}
