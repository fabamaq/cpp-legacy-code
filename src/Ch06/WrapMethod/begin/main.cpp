#include "main.hpp"

/**
 * @brief We are adding up daily timecards for an employee and then
 * sending his payment information to a PayDispatcher.
 */
void Employee::pay() {
	Money amount = Money();
	// for (Iterator it = timecards.iterator(); it.hasNext();)
	for (auto it = timecards.begin(); it != timecards.end(); ++it) {
		Timecard card = *it;
		if (payPeriod.contains(date)) {
			amount.add(card.getHours() * payRate);
		}
	}
	payDispatcher.pay(this, date, amount);

	// TODO:
	// ... Every time we pay an employee,
	// ... we have to update a file with the employee's name
	// ... so that it can be sent off to some reporting software
}

/**
 * @note we could do it (implicitly) like this

class Employee {
  private:
	void dispatchPayment() {
		Money amount = Money();
		for (auto it = timecards.begin(); it != timecards.end(); ++it) {
			Timecard card = (Timecard)*it;
			if (payPeriod.contains(date)) {
				amount.add(card.getHours() * payRate);
			}
		}
		payDispatcher.pay(this, date, amount);
	}

* @note we extract the behavior into a private method and add new log feature
* ... to the public interface
  public:
	void pay() {
		logPayment();
		dispatchPayment();
	}
}

 * @note or we could do it (explicitly) like this

 class Employee {
  private:
	void logPayment() {
	  ...
	}

  public:
  void makeLoggedPayment() {
	  logPayment();
	pay();
	}

	void pay() {
	...
	}
}

 */
