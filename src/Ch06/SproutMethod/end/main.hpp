#pragma once

#include <algorithm>
#include <iostream>
#include <list>
using std::list;

struct Entry {
	void postDate() { std::cout << "postDate()\n"; }
};

bool operator==(const Entry & /*lhs*/, const Entry & /*rhs*/) { return true; }
bool operator<(const Entry & /*lhs*/, const Entry & /*rhs*/) { return true; }

class TransactionBundle {
  public:
	list<list<Entry>> entriesList;
	list<list<Entry>> getListManager() { return entriesList; }
};

TransactionBundle gTransactionBundle;