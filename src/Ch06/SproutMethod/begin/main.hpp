#pragma once

#include <chrono>
#include <iostream>
#include <list>
using std::list;

struct Entry {
	void postDate() { std::cout << time(NULL); }
};

class TransactionBundle {
  public:
	list<list<Entry>> entriesList;
	list<list<Entry>> getListManager() { return entriesList; }
};

TransactionBundle gTransactionBundle;