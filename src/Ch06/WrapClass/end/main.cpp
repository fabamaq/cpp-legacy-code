/**
 * @brief Wrap Class
 * 1. Identify a method where you need to make a change
 * 2. If the change can be formulated as a single sequence of statements in one
 * place,
 * ... create a class that accepts the class you are going to wrap as a
 * constructor argument.
 * ... If you have trouble creating a class that wraps the original class in a
 * test harness,
 * ... you might have to use @sa Extract Implementer or @sa Extract Interface on
 * the wrapped class
 * ... so that you can instantiate your wrapper
 * 3. Create a method on that class, using TDD, that does the new work.
 * ... Write another method that calls the new method and the old method on the
 * wrapped class.
 * 4. Instantiate the wrapper class in your code in the place where you need to
 * enable the new behaviour
 */

#include "main.hpp"

/// @note we used Extract Implementer to turn the Employee class into an
/// interface
/// @implements Decorator design pattern
class LoggingEmployee : public Employee {
  public:
	LoggingEmployee(Employee &e) : employee(e) {}

	void pay() {
		logPayment();
		employee.pay();
	}

  private:
	void logPayment() {
		// ...
	}

  private:
	Employee &employee;
};

/// @brief Logs information about the payment without decorating the Employee
class LoggingPayDispatcher {
	Employee &employee;

  public:
	LoggingPayDispatcher(Employee &e) : employee(e) {}
	void pay() {
		employee.pay();
		logPayment();
	}

  private:
	void logPayment() {}
};
