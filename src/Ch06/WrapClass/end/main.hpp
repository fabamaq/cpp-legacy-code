#pragma once

#include <ctime>

#include <algorithm>
#include <list>
#include <map>
#include <vector>

class Money {
	double amount;

  public:
	void add(double value) { amount += value; }
};

class Timecard {
	double hours;

  public:
	double getHours() { return hours; }
};

inline bool operator==(const tm &lhs, const tm &rhs) {
	// clang-format off
	if (lhs.tm_sec != rhs.tm_sec) return false;
	if (lhs.tm_min != rhs.tm_min) return false;
	if (lhs.tm_hour != rhs.tm_hour) return false;
	if (lhs.tm_mday != rhs.tm_mday) return false;
	if (lhs.tm_mon != rhs.tm_mon) return false;
	if (lhs.tm_year != rhs.tm_year) return false;
	// clang-format on
	return true;
}

template <class T> class HashList : std::list<T> {
  public:
	bool contains(T value) {
		auto it = std::find(this->begin(), this->end(), value);
		return it != this->end();
	}
};

/// @note
/// @brief Employee's interface
class Employee {
  public:
	virtual void pay() = 0;
	virtual ~Employee() = default;
};

class PayDispatcher {
  public:
	void pay(Employee *employee, tm date, Money amount) {
		(void)employee;
		(void)date;
		(void)amount;
	}
};

/// @brief Abstract Employee
class ConcreteEmployee : public Employee {
	std::vector<Timecard> timecards;
	HashList<tm> payPeriod;
	tm date;
	double payRate;

	PayDispatcher payDispatcher;

  public:
	void pay() {
		Money amount = Money();
		// for (Iterator it = timecards.iterator(); it.hasNext();)
		for (auto it = timecards.begin(); it != timecards.end(); ++it) {
			Timecard card = *it;
			if (payPeriod.contains(date)) {
				amount.add(card.getHours() * payRate);
			}
		}
		payDispatcher.pay(this, date, amount);
	}
};
