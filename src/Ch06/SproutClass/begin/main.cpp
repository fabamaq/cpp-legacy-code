#include "main.hpp"

class QuarterlyReportGenerator {
	int beginDate;
	int endDate;
	struct DB {
		std::vector<Result> queryResults(int b, int e) {
			(void)b;
			(void)e;
			return std::vector<Result>();
		}
	} database;

  public:
	std::string generate();
};

/// @todo add a header row for the HTML table
// The header should look something like this:
// "<tr><td>Department</td><td>Manager</td><td>Profit</td><td>Expenses</td></tr>"
std::string QuarterlyReportGenerator::generate() {
	std::vector<Result> results = database.queryResults(beginDate, endDate);

	std::string pageText;

	pageText += "<html><head><title>"
				"Quarterly Report"
				"</title></head><body><table>";

	if (results.size() != 0) {
		for (std::vector<Result>::iterator it = results.begin();
			 it != results.end(); ++it) {
			pageText += "<tr>";
			pageText += "<td>" + it->department + "</td>";
			pageText += "<td>" + it->manager + "</td>";
			char buffer[128];
			sprintf(buffer, "<td>$%d</td>", it->netProfit / 100);
			pageText += std::string(buffer);
			sprintf(buffer, "<td>$%d</td>", it->operatingExpense / 100);
			pageText += std::string(buffer);
			pageText += "</tr>";
		}
	} else {
		pageText += "No results for this period";
	}
	pageText += "</table>";
	pageText += "</body>";
	pageText += "</html>";

	return pageText;
}
