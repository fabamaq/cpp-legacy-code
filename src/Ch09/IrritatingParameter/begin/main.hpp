#pragma once
#include "StandardLibrary.hpp"

using boolean = bool;
using std::string;

using String = std::string;

constexpr int DEFAULT_PORT = 22;

struct RGHConnection {
	RGHConnection(int port, String Name,
				  string passwd) /* throws IOException */ {
		(void)port;
		(void)Name;
		(void)passwd;
		// Connect to a server
	}
};

// Policiy for credit decisions
struct CreditMaster {
	CreditMaster(String filename, boolean isLocal) {
		(void)filename;
		(void)isLocal;
		// ...
	}
};

struct Certificate {
	/* data */
};

struct Customer {
	/* data */
};

struct InvalidCredit {
	/* data */
};
