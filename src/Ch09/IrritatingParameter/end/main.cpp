#include "main.hpp"

#include "catch2/catch.hpp"

struct FakeConnection : public IRGHConnection {
	~FakeConnection() = default;
	FakeConnection() = default;
	FakeConnection(const char *username, const char *password) {
		(void)username;
		(void)password;
	}

	RFDIReport report;

	void connect() override {}
	void disconnect() override {}
	RFDIReport RGDIReportFor(int id) override {
		(void)id;
		return report;
	}
	ACTIOReport *ACTIOReportFor(int customerID) override {
		(void)customerID;
		return nullptr;
	}
};

/**
 * @brief
 * - Tell whether customers have valid credit.
 * - ... if true, get a certificate for credit amount
 * - ... it false, throw InvalidCredit
 *
 * TODO: add a new getValidationPercent method
 * - to tell the % of successful validateCustomer calls
 * - ... we've made over the life of the validator
 */
class CreditValidator {
  public:
	CreditValidator(IRGHConnection *connection, CreditMaster master,
					String validatorID) {
		(void)connection;
		(void)master;
		(void)validatorID;
		// ...
	}

	Certificate validateCustomer(Customer customer) /* throws InvalidCredit */ {
		// ...
		(void)customer;
		return {};
	}
};

TEST_CASE("The Case of the Irritating Parameter", "[testNoSuccess][end]") {
	CreditMaster master = CreditMaster("crm2.mas", true);
	FakeConnection *connection = new FakeConnection();
	CreditValidator validator = CreditValidator(connection, master, "a");

	connection->report = RFDIReport();
	Certificate result = validator.validateCustomer(Customer());
	CHECK(Certificate::VALID == result.getStatus());

	delete connection;
}

TEST_CASE("The Case of the Irritating Parameter",
		  "[testAllPassed100Percent][end]") {
	CreditMaster master = CreditMaster("crm2.mas", true);
	FakeConnection *connection = new FakeConnection("admin", "rii8ii9s");
	CreditValidator validator = CreditValidator(connection, master, "a");

	connection->report = RFDIReport();
	Certificate result = validator.validateCustomer(Customer());
	CHECK(100 == result.getValidationPercent());

	delete connection;
}
