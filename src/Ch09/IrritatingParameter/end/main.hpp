#pragma once
#include "StandardLibrary.hpp"

using boolean = bool;
using std::string;

using String = std::string;

constexpr int DEFAULT_PORT = 22;

struct RFDIReport {};
struct ACTIOReport {};
struct RFPacket {};

struct IRGHConnection {
	virtual ~IRGHConnection() = default;
	virtual void connect() = 0;
	virtual void disconnect() = 0;
	virtual RFDIReport RGDIReportFor(int id) = 0;
	virtual ACTIOReport *ACTIOReportFor(int customerID) = 0;
};

struct RGHConnection : public IRGHConnection {
	RGHConnection(int port, String Name,
				  string passwd) /* throws IOException */ {
		(void)port;
		(void)Name;
		(void)passwd;
		// Connect to a server
	}

	void connect() override;
	void disconnect() override;
	RFDIReport RGDIReportFor(int id) override;
	ACTIOReport *ACTIOReportFor(int customerID) override;

  private:
	void retry();
	RFPacket formPacket();
};

// Policiy for credit decisions
struct CreditMaster {
	CreditMaster(String filename, boolean isLocal) {
		(void)filename;
		(void)isLocal;
		// ...
	}
};

struct Certificate {
	constexpr static int VALID = 7;
	int getStatus() { return VALID; }
	int getValidationPercent() { return 100; }
};

struct Customer {
	/* data */
};

struct InvalidCredit {
	/* data */
};
