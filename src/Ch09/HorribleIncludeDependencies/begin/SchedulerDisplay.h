#pragma once

#include <string>
using std::string;

class SchedulerDisplay {
  public:
    void displayEntry(const string& entityDescription);
};
