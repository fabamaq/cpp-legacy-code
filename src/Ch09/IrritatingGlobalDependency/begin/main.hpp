#pragma once

#include <string>
using String = std::string;

struct PermitNotice {
	PermitNotice() = default;
	PermitNotice(int code, String name) : mCode(code), mName(std::move(name)) {}
	bool isValid() { return true; }

  private:
	int mCode{};
	String mName{};
};

struct Permit {
	Permit() = default;
	Permit(PermitNotice notice) : mNotice(notice) {}
	bool isValid() { return true; }

	void validate() {}

	PermitNotice mNotice;
};

/**
 * @brief The Irritating Global Dependency
 * @implements the Singleton design pattern
 */
class PermitRepository {
	static PermitRepository *sInstance;

  public:
	static PermitRepository &getInstance() {
		if (sInstance == nullptr) {
			sInstance = new PermitRepository();
		}
		return *sInstance;
	}

	Permit findAssociatedPermit(PermitNotice notice) { return Permit(notice); }

  private:
	PermitRepository() = default;
};

inline PermitRepository *PermitRepository::sInstance = nullptr;
