#pragma once

#include <string>
using String = std::string;

struct PermitNotice {
	PermitNotice() = default;
	PermitNotice(int newCode, String newName) : code(newCode), name(newName) {}
	bool isValid() { return true; }

	int code;
	String name;
};

struct Permit {
	Permit() = default;
	Permit(PermitNotice notice) : mNotice(notice) {}
	bool isValid() { return true; }

	void validate() {}

	PermitNotice mNotice;
};

struct PermitNoticeHash {
	std::size_t operator()(const PermitNotice &k) const {
		return std::hash<int>{}(k.code);
	}
};

struct PermitNoticeKeyEqual {
	bool operator()(const PermitNotice &lhs, const PermitNotice &rhs) const {
		return lhs.code == rhs.code;
	}
};

#include <unordered_map>
//          std::unordered_map<Key,T,Hash,KeyEqual,[Allocator]>::unordered_map
using Map = std::unordered_map<PermitNotice, Permit, PermitNoticeHash,
							   PermitNoticeKeyEqual>;

/**
 * @brief The Irritating Global Dependency
 * @implements the Singleton design pattern
 */
class PermitRepository {
	static PermitRepository *sInstance;

  public:
	static PermitRepository &getInstance() {
		if (sInstance == nullptr) {
			sInstance = new PermitRepository();
		}
		return *sInstance;
	}

	static void setTestingInstance(PermitRepository *newInstance) {
		sInstance = newInstance;
	}

	Permit findAssociatedPermit(PermitNotice notice) {
		// open permit database

		// select using values in notice

		// verify we have only one  matching permit, if not report error

		// return matching permit
		return Permit(notice);
	}

  protected:
	PermitRepository() = default;
};

inline PermitRepository *PermitRepository::sInstance = nullptr;
