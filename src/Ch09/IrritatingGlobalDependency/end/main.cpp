#include "main.hpp"

#include <catch2/catch.hpp>
#include <stdexcept>

class Facility {
	Permit basePermit;

  public:
	class PermitViolation : public std::runtime_error {
	  public:
		explicit PermitViolation(Permit p)
			: std::runtime_error("PermitViolation") {
			(void)p;
		}
	};

	Facility(int facilityCode, String owner,
			 PermitNotice notice) /* throws PermitViolation */ {
		(void)facilityCode;
		(void)owner;
		Permit associatedPermit =
			PermitRepository::getInstance().findAssociatedPermit(notice);

		if (associatedPermit.isValid() && !notice.isValid()) {
			basePermit = associatedPermit;
		} else if (!notice.isValid()) {
			Permit permit = Permit(notice);
			permit.validate();
			basePermit = permit;
		} else {
			throw new PermitViolation(associatedPermit /* before::permit */);
		}
	}
	// ...

	constexpr static int RESIDENCE = 1;
};

class TestingPermitRepository : public PermitRepository {
	Map permits{};

  public:
	TestingPermitRepository() : PermitRepository() {}

	void addAssociatedPermit(PermitNotice notice, Permit permit) {
		permits.emplace(std::make_pair(notice, permit));
	}

	Permit findAssociatedPermit(const PermitNotice &notice) {
		return permits.at(notice);
	}
};

TEST_CASE("The Case of the Irritating Global Dependency",
		  "[testCreate][begin]") {
	// Setup
	TestingPermitRepository *repository = new TestingPermitRepository();

	// ...
	// add permits to the repository here
	// ...

	PermitRepository::setTestingInstance(repository);

	SECTION("testCreate") {
		PermitNotice notice = PermitNotice(0, "a");
		Facility facility = Facility(Facility::RESIDENCE, "b", notice);
	}
}
