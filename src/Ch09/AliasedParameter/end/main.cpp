#include "main.hpp"

#include <catch2/catch.hpp>

class IndustrialFacility : public Facility {
  public:
	IndustrialFacility(int facilityCode, String owner,
					   IOriginationPermit &permit) /* throws PermitViolation */
		: Facility(facilityCode, owner) {

		IPermit associatedPermit =
			PermitRepository::GetInstance().findAssociatedFromOrigination(
				permit);

		if (associatedPermit.isValid() && !permit.isValid()) {
			basePermit = associatedPermit;
		} else if (!permit.isValid()) {
			permit.validate();
			basePermit = permit;
		} else {
			throw PermitViolation(permit);
		}
	}

  private:
	IPermit basePermit;
};

TEST_CASE("The Case of the Aliased Parameter", "[end]") {

	SECTION("testHasPermits") {

		class AlwaysValidPermit : public FakeOriginationPermit {
		  public:
			void validate() override {
				// set the validation flag
				becomeValid();
			}
		};

		AlwaysValidPermit aPermit;
		Facility *facility =
			new IndustrialFacility(Facility::HT_1, "b", aPermit);

		CHECK(facility->hasPermits());

		delete facility;
	}
}
