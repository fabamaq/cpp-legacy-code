#pragma once

#include "StandardLibrary.hpp"

using String = std::string;

/// ===========================================================================
/// @section Permit
/// ===========================================================================

class IPermit {
  public:
	virtual ~IPermit() = default;
	virtual void validate() {}
	virtual bool isValid() { return true; }
};

class Permit : public IPermit {
  public:
	virtual ~Permit() = default;
};

class PermitViolation : public std::runtime_error {
  public:
	PermitViolation(IPermit &aPermit) : std::runtime_error("PermitViolation") {
		(void)aPermit;
	}
};

class IFacilityPermit : public IPermit {
  public:
	virtual ~IFacilityPermit() = default;
};

class FacilityPermit : public Permit, public IFacilityPermit {
  public:
	virtual ~FacilityPermit() = default;
	void validate() override {}
};

class IOriginationPermit : public IFacilityPermit {
  public:
	virtual ~IOriginationPermit() = default;
};

class OriginationPermit : public FacilityPermit, public IOriginationPermit {
  public:
	~OriginationPermit() override = default;

	void validate() override {
		// form connectino to database
		// query for validation information
		// set the validation flag
		// close database
	}
};

class FakeOriginationPermit : public FacilityPermit, public IOriginationPermit {
  protected:
	void becomeValid() {}

  public:
	virtual ~FakeOriginationPermit() = default;
	void validate() override {}
};

class PermitRepository {
  public:
	static PermitRepository &GetInstance() {
		static PermitRepository sInstance;
		return sInstance;
	}

	IPermit findAssociatedFromOrigination(IPermit &aPermit) {
		(void)aPermit;
		return {};
	}
};

/// ===========================================================================
/// @section Facility
/// ===========================================================================

class Facility {
  public:
	static const int HT_1 = 1;

	virtual ~Facility() = default;
	Facility(int facilityCode, String owner)
		: mFacilityCode(facilityCode), mOwner(std::move(owner)) {}

	bool hasPermits() { return true; }

  private:
	int mFacilityCode;
	String mOwner;
};
