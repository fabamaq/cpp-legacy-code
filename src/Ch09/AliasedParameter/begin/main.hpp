#pragma once

#include "StandardLibrary.hpp"

using String = std::string;

/// ===========================================================================
/// @section Permit
/// ===========================================================================
class Permit {
  public:
	virtual ~Permit() = default;
	bool isValid() { return true; }
	void validate() {}
};

class PermitViolation : public std::runtime_error {
  public:
	PermitViolation(Permit &aPermit) : std::runtime_error("PermitViolation") {
		(void)aPermit;
	}
};

class FacilityPermit : public Permit {
  public:
	virtual ~FacilityPermit() = default;
};

class OriginationPermit : public FacilityPermit {
  public:
	~OriginationPermit() override = default;

	void validate() {
		// form connectino to database
		// query for validation information
		// set the validation flag
		// close database
	}
};

class PermitRepository {
  public:
	static PermitRepository &GetInstance() {
		static PermitRepository sInstance;
		return sInstance;
	}

	Permit findAssociatedFromOrigination(Permit &aPermit) {
		(void)aPermit;
		return {};
	}
};

/// ===========================================================================
/// @section Facility
/// ===========================================================================

class Facility {
  public:
	virtual ~Facility() = default;
	Facility(int facilityCode, String owner)
		: mFacilityCode(facilityCode), mOwner(std::move(owner)) {}

  private:
	int mFacilityCode;
	String mOwner;
};
