#include "main.hpp"

#include <catch2/catch.hpp>

class SerialTask {
  public:
	virtual ~SerialTask() = default;
	virtual void run();
	// ...
};

class ISchedulingTask {
  public:
	virtual ~ISchedulingTask() = default;
	virtual void run() = 0;
	// ...
};

class SchedulerPane {
  public:
	virtual ~SchedulerPane() = default;
};

class SchedulingTask : public SerialTask, public ISchedulingTask {
  public:
	~SchedulingTask() = default;
	SchedulingTask(Scheduler &aScheduler, MeetingResolver &aMeetingResolver)
		: mScheduler(aScheduler), mMeetingResolver(aMeetingResolver) {
		// ...
	}

	virtual void run() { SerialTask::run(); }

	Scheduler &mScheduler;
	MeetingResolver &mMeetingResolver;
};

class SchedulingTaskPane : public SchedulerPane {
  public:
	~SchedulingTaskPane() = default;
	SchedulingTaskPane(SchedulingTask &task) : mSchedulingTask(task) {
		// ...
	}

	SchedulingTask &mSchedulingTask;
};

TEST_CASE("The Case of the Onion Parameter", "[begin]") {
	// Nightmare to isntantiate SchedulingTaskPane
}
