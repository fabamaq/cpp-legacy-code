#include "main.hpp"

#include <catch2/catch.hpp>

class SchedulerPane {
  public:
	virtual ~SchedulerPane() = default;
};

class SchedulingTask : public SerialTask {
  public:
	~SchedulingTask() = default;
	SchedulingTask(Scheduler &aScheduler, MeetingResolver &aMeetingResolver)
		: mScheduler(aScheduler), mMeetingResolver(aMeetingResolver) {
		// ...
	}

	Scheduler &mScheduler;
	MeetingResolver &mMeetingResolver;
};

class SchedulingTaskPane : public SchedulerPane {
  public:
	~SchedulingTaskPane() = default;
	SchedulingTaskPane(SchedulingTask &task) : mSchedulingTask(task) {
		// ...
	}

	SchedulingTask &mSchedulingTask;
};

TEST_CASE("The Case of the Onion Parameter", "[begin]") {
	// Nightmare to isntantiate SchedulingTaskPane
}
