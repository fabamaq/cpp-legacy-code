#include "main.hpp"

#include "catch2/catch.hpp"

class mailing_list_dispatcher {
  public:
	mailing_list_dispatcher();
	virtual ~mailing_list_dispatcher() = default;

	void send_message(const std::string &message);
	void add_recipient(const mail_txm_id id, const mail_address &address);

  private:
	mail_service *service;
	int status;
};

mailing_list_dispatcher::mailing_list_dispatcher()
	: service(new mail_service), status(MAIL_OKAY) {
	const int client_type = 12;
	service->connect();
	if (service->get_status() == MS_AVAILABLE) {
		service->register_(this, client_type, MARK_MESSAGES_OFF);
		service->set_param(client_type, ML_NOBOUND | ML_REPEATOFF);
	} else {
		status = MAIL_OFFLINE;
	}
}

TEST_CASE("The Case of the Hidden Dependency", "[begin]") { SUCCEED(); }
