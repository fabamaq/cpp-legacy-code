#include "main.hpp"

#include <catch2/catch.hpp>

class WatercolorPane {
  public:
	WatercolorPane(Form *border, WashBrush *brush, Pattern *backdrop) {
		// ...
		anteriorPanel = new Panel(border);
		anteriorPanel->setBorderColor(brush->getForeColor());
		backgroundPanel = new Panel(border, backdrop);

		cursor = new FocusWidget(brush, backgroundPanel);

		// ...
	}
	// ...

	int getComponentCount() { return 0; }

  private:
	Panel *anteriorPanel;
	Panel *backgroundPanel;
	FocusWidget *cursor;
};

struct TestingFocusWidget {
	TestingFocusWidget() = default;
};

TEST_CASE("The Case of the Construction Blob",
		  "[renderBorder]}[WatercolorPane][begin]") {
	Form *form = new Form();
	WashBrush *border = new WashBrush();
	Pattern *backdrop = new Pattern();

	TestingFocusWidget *widget = new TestingFocusWidget();
	WatercolorPane pane(form, border, backdrop);

	// TODO
	// pane.supersedeCursor(widget);

	CHECK(0 == pane.getComponentCount());

	delete widget;
	delete backdrop;
	delete border;
	delete form;
}
