#pragma once

struct Color {};

struct Form {};

struct WashBrush {
	Color getForeColor() { return Color(); }
};

struct Pattern {};

struct Panel {
	Panel(Form *f) : form(f) {}

	Panel(Form *f, Pattern *p) : form(f), pattern(p) {}

	void setBorderColor(Color c) { color = c; }

	Form *form;
	Pattern *pattern;
	Color color;
};

struct FocusWidget {
	FocusWidget(WashBrush *b, Panel *p) : brush(b), panel(p) {}
	WashBrush *brush;
	Panel *panel;
};