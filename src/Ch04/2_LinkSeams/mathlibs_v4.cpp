#include "mathlibs_v4.h"
#include "mathlibs_v4_extra.h"

#include "StandardLibrary.hpp"

/// ===========================================================================
/// @brief mathlibs_v4_h
/// ===========================================================================

int global_protocol_version{5}; // NOLINT

void isDebug(int *pointer) { *pointer = 0; }

/// ---------------------------------------------------------------------------
/// @brief getInputRequirements
/// ---------------------------------------------------------------------------
void getProtocolVersion(int *pointer) { *pointer = global_protocol_version; }

/// ---------------------------------------------------------------------------
/// @brief getInputRequirements
/// ---------------------------------------------------------------------------
int getInputRequirements() {
	return GML_InputRequirementsKeys::game_type |
		   //    GML_InputRequirementsKeys::is_mega_active |
		   //    GML_InputRequirementsKeys::sv_multi_progressive |
		   GML_InputRequirementsKeys::current_credits;
}

/// ---------------------------------------------------------------------------
/// @brief getParameters
/// ---------------------------------------------------------------------------
struct DummyParameters {
	int kActualBallUniverseSize{60};
	int kDrawBalls{30};
	int kExtraBalls{10};
	int kCards{20};
	int kCells{15};
	int kRows{3};
	int kColumns{5};
	int kPrizes{19};
	int kBallUniverseMin{1};
	int kBallUniverseMax{90};
	int kGenerateSingleCard{0};
	int kClientCanSetCards{0};
	int kTotalNumberOfSpecialPrizes{13};
	int kJackpotPrizesMask{262144};
	int kBingoPrizesMask{262144};
	int kSpecialPrizesMask{65536};
	int kNumOfIntsToEncodeCards{1};
	int kNumOfIntsToEncodeCells{1};
	int kNumOfIntsToEncodePrizes{1};
	int kNumOfIntsToEncodeSpecialPrizes{1};
	int kGameType{1};
	int _unused_SpinParameter{0};
	int kWildBallNumber{222};
	int kHasWildBall{1};
	int kIsMathDebug{0};
	int kReturnToPlayer{9290};
	int kDobraMaxRepetitions{0};
	int kDobraMaxValue{0};
	int kIsHighestPrizeOnly{0};
	int kMultiplierProfiles{1};
	int kHasGoldenBall{0};
	int kHasMegaWin{0};
	int kHasMegaBingo{0};
	int kHasMegaBonus{0};
	int kHasMegaSales{0};
	int kHasMultiPots{0};
	int kAllowedDenominations{3};
	int kHasDiscard{0};
	int kSuperWildBallNumber{0};
	int kHasSuperWildBall{0};
	int kSpecialBallType{0};
	int kNumOfFlashBingoGameModes{0};
	int kNumOfFlashBingoCardModes{0};
	int kNumOfJumboLinkSymbols{0};
	int kPricePerOpenCard{1};
	int kHasSlotBingoBonus{0};
	int kHasDynamicExtraBalls{0};
	int HasServerMultiPots{0};
	int IsMultiplayerMath{0};
	int kMaxBet{10};
	int IsSetGameSeedRequired{0};
};

DummyParameters &GetDummyParameters() {
	static DummyParameters parameters{};
	return parameters;
}

void getParameters(int *buffer) {
	/// @note this works because DummyParameters is a POD struct
	constexpr auto size = sizeof(DummyParameters) / sizeof(int);
	const auto &parameters = GetDummyParameters();
	memcpy(buffer, &parameters, size);
}

/// ---------------------------------------------------------------------------
/// @brief getVersion
/// ---------------------------------------------------------------------------
void getVersion(char *buffer) {
	constexpr int cxMathNameMaxSize{64};
	static const std::string dummyMathName = "MATH-MOCKLIB-PT-1.0.0-DEV-32";
	std::vector<int> data(cxMathNameMaxSize * sizeof(char) / sizeof(int));
	memcpy(data.data(), dummyMathName.c_str(),
		   dummyMathName.size() * sizeof(char));
	memcpy(buffer, data.data(), cxMathNameMaxSize * sizeof(char));
}

/// ---------------------------------------------------------------------------
/// @brief getAllowedDenominations
/// ---------------------------------------------------------------------------
void getAllowedDenominations(int *buffer) { buffer[0] = 100; }

/// ---------------------------------------------------------------------------
/// @brief getPrizeTable
/// ---------------------------------------------------------------------------
void getPrizeTable(int *buffer) {
	buffer[0] = 1500; // BINGO
}

/// ---------------------------------------------------------------------------
/// @brief getPayTable
/// ---------------------------------------------------------------------------
void getPayTable(int *buffer) {
	buffer[0] = 2000; // BINGO + BONUS
}

/// ---------------------------------------------------------------------------
/// @brief getPrizeGroup
/// ---------------------------------------------------------------------------
void getPrizeGroup(int *buffer) { buffer[0] = 11; }

/// ---------------------------------------------------------------------------
/// @brief getPrizeMasks
/// ---------------------------------------------------------------------------
void getPrizeMasks(int *buffer) { buffer[0] = 32726; }

/// ---------------------------------------------------------------------------
/// @brief getPrizeInfo
/// ---------------------------------------------------------------------------
void getPrizeInfo(int *jackpotBuffer, int *bingoBuffer, int *bonusBuffer) {
	jackpotBuffer[0] = 1; // random value
	bingoBuffer[0] = 1;	  // random value
	bonusBuffer[0] = 1;	  // random value
}

/// ---------------------------------------------------------------------------
/// @brief getSpecialPrizeInfo
/// ---------------------------------------------------------------------------
void getSpecialPrizeInfo(int *buffer) { buffer[0] = 1; }

/// ---------------------------------------------------------------------------
/// @brief getMultiplierProfiles
/// ---------------------------------------------------------------------------
void getMultiplierProfiles(int *buffer) { buffer[0] = 1; }

/// ---------------------------------------------------------------------------
/// @brief getMegaBingoValues
/// ---------------------------------------------------------------------------
void getMegaBingoValues(int *buffer, int bet) {
	buffer[0] = 6000; // MEGA BINGO
}

/// ---------------------------------------------------------------------------
/// @brief getMegaPlayCost
/// ---------------------------------------------------------------------------
void getMegaPlayCost(int *buffer) {
	buffer[0] = -1;
	buffer[1] = -1;
	buffer[2] = -1;
	buffer[3] = 1;
}

/// ---------------------------------------------------------------------------
/// @brief getCards
/// ---------------------------------------------------------------------------
void getCards(int *buffer) { ; }

/// ---------------------------------------------------------------------------
/// @brief getCardSeed
/// ---------------------------------------------------------------------------
void getCardSeed(long unsigned int *seed1, long unsigned int *seed2) { ; }

/// ---------------------------------------------------------------------------
/// @brief getGameSeed
/// ---------------------------------------------------------------------------
void getGameSeed(long unsigned int *seed1, long unsigned int *seed2) { ; }

/// ---------------------------------------------------------------------------
/// @brief setGameSeed
/// ---------------------------------------------------------------------------
void setGameSeed(long unsigned int seed1, long unsigned int seed2) { ; }

/// ---------------------------------------------------------------------------
/// @brief setCardSeed
/// ---------------------------------------------------------------------------
void setCardSeed(long unsigned int seed1, long unsigned int seed2) { ; }

/// ---------------------------------------------------------------------------
/// @brief rotateSeed
/// ---------------------------------------------------------------------------
void rotateSeed(void) { ; }

/// ---------------------------------------------------------------------------
/// @brief getSpecialGame
/// ---------------------------------------------------------------------------
void getSpecialGame(int special_prize_type, int bet, int denomination,
					int *r_aditional_info) {
	;
}

/// ---------------------------------------------------------------------------
/// @brief endGameState
/// ---------------------------------------------------------------------------
void endGameState(int *endState) { ; }

/// ---------------------------------------------------------------------------
/// @brief getGameAfterDiscard
/// ---------------------------------------------------------------------------
void getGameAfterDiscard(int *r_output, int *cards, int bet, int denomination,
						 int *open_cards, bool eligible, int *additional_info,
						 int balls_played, int *balls) {
	;
}

/// ---------------------------------------------------------------------------
/// @brief refreshExtraBallCost
/// ---------------------------------------------------------------------------
void refreshExtraBallCost(int *r_output, int *p_cards, int bet,
						  int denomination, int *open_cards, bool eligible,
						  int *additional_info, int balls_played,
						  int *p_balls) {}

/// ---------------------------------------------------------------------------
/// @brief setMultiProgressiveBaseValue
/// ---------------------------------------------------------------------------
void setMultiProgressiveBaseValue(int value) { ; }

/// ---------------------------------------------------------------------------
/// @brief gameStateAfterRestore
/// ---------------------------------------------------------------------------
void gameStateAfterRestore(int value) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusPrizeGroup
/// ---------------------------------------------------------------------------
void getBingoBonusPrizeGroup(int *p_prize_group) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusPrizeMask
/// ---------------------------------------------------------------------------
void getBingoBonusPrizeMask(int *p_prize_mask) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusPrizeTable
/// ---------------------------------------------------------------------------
void getBingoBonusPrizeTable(int *p_prize_table) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusParameters
/// ---------------------------------------------------------------------------
void getBingoBonusParameters(int *p_parameters) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusSpecialPrizeInfo
/// ---------------------------------------------------------------------------
void getBingoBonusSpecialPrizeInfo(int *p_special_prize_info) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusNumberOfPlaysInformation
/// ---------------------------------------------------------------------------
void getBingoBonusNumberOfPlaysInformation(int *p_prize_table) { ; }

/// ---------------------------------------------------------------------------
/// @brief getBingoBonusSpecialGame
/// ---------------------------------------------------------------------------
void getBingoBonusSpecialGame(int *p_output, int *p_cards, int bet,
							  int denomination) {
	;
}

/// ---------------------------------------------------------------------------
/// @brief getEasyLinkValues
/// ---------------------------------------------------------------------------
void getEasyLinkValues(int *p_output) { ; }

/// ---------------------------------------------------------------------------
/// @brief getFlashBingoDrawSizes
/// ---------------------------------------------------------------------------
void getFlashBingoDrawSizes(int *output) { ; }

/// ---------------------------------------------------------------------------
/// @brief getFlashBingoDrawCostsForCardMode
/// ---------------------------------------------------------------------------
void getFlashBingoDrawCostsForCardMode(int *output, int number_of_cards) { ; }

/// ---------------------------------------------------------------------------
/// @brief getFlashBingoCardModes
/// ---------------------------------------------------------------------------
void getFlashBingoCardModes(int *output) { ; }

/// ---------------------------------------------------------------------------
/// @brief getMiniSlotsBonusParameters
/// ---------------------------------------------------------------------------
void getMiniSlotsBonusParameters(int *output) { ; }

/// ---------------------------------------------------------------------------
/// @brief getMiniSlotsBonusPrizeTable
/// ---------------------------------------------------------------------------
void getMiniSlotsBonusPrizeTable(int *output) { ; }

/// ---------------------------------------------------------------------------
/// @brief getMiniSlotsBonus
/// ---------------------------------------------------------------------------
void getMiniSlotsBonus(int *output, int input) { ; }

/// ---------------------------------------------------------------------------
/// @brief gmlWriteTcpMathConfigDataInto
/// ---------------------------------------------------------------------------
void gmlWriteTcpMathConfigDataInto(uint32_t (&buffer)[10]) { ; }

/// ---------------------------------------------------------------------------
/// @brief gmlWriteTcpPlayQuixantDataInto
/// ---------------------------------------------------------------------------
void gmlWriteTcpPlayQuixantDataInto(uint32_t (&buffer)[10], int openedCards,
									int betLevel, int denomination) {
	;
}

/// ---------------------------------------------------------------------------
/// @brief gmlOnReceiveTcpPlayQuixantResponse
/// ---------------------------------------------------------------------------
void gmlOnReceiveTcpPlayQuixantResponse(int *mathN) { ; }

/// ---------------------------------------------------------------------------
/// @brief gmlReportCashout
/// ---------------------------------------------------------------------------
void gmlReportCashout() { ; }

/// ===========================================================================
/// @brief mathlibs_v4_extra_h
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @brief setProtocolVersion
/// ---------------------------------------------------------------------------
void setProtocolVersion(int newVersion) {
	global_protocol_version = newVersion;
}
