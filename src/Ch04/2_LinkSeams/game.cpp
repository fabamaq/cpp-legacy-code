// FMQ Math Library
#include "mathlibs_v4.h"

// C++ Standard Library
#include "StandardLibrary.hpp"

// POSIX Library
#include <dlfcn.h>

/// ===========================================================================
/// @brief SharedLibrary
/// ===========================================================================
class SharedLibrary {
  public:
	SharedLibrary(const char *path) {
		mHandle = dlopen(path, RTLD_LAZY);
		if (!mHandle) {
			fprintf(stderr, "Failed to load library with error: %s\n",
					dlerror());
			throw std::runtime_error("SharedLibrary Initialization Error!");
		}
		dlerror(); /* Clear any existing error */
	}
	~SharedLibrary() { dlclose(mHandle); }

	void *getHandle() { return mHandle; }

  private:
	void *mHandle;
};

/// ===========================================================================
/// @brief Application entry point
/// ===========================================================================
int main() {
	SharedLibrary mathLib("./libMath.so");
	auto handle = mathLib.getHandle();

	// ------------------------------------------------------------------------
	// Get Protocol Version
	// ------------------------------------------------------------------------
	auto getProtocolVersionFn =
		(void (*)(int *))dlsym(handle, "getProtocolVersion");

	char *error = nullptr;
	if ((error = dlerror()) != nullptr) {
		fprintf(stderr,
				"Failed to load function 'getProtocolVersion' with error: %s\n",
				error);
		return EXIT_FAILURE;
	}

	int protocolVersion = 0;
	if (getProtocolVersionFn) {
		getProtocolVersionFn(&protocolVersion);
	} else {
		fprintf(stderr, "Failed to call 'getProtocolVersion'\n");
		return EXIT_FAILURE;
	}

	printf("Protocol version: %d\n", protocolVersion);

	// ------------------------------------------------------------------------
	// Set New Protocol Version
	// ------------------------------------------------------------------------
	auto setProtocolVersionFn =
		(void (*)(int))dlsym(handle, "setProtocolVersion");

	if ((error = dlerror()) != nullptr) {
		fprintf(stderr,
				"Failed to load function 'setProtocolVersion' with error: %s\n",
				error);
		return EXIT_FAILURE;
	}

	int newVersion = 7;
	if (setProtocolVersionFn) {
		setProtocolVersionFn(newVersion);
	} else {
		fprintf(stderr, "Failed to call 'setProtocolVersion'\n");
		return EXIT_FAILURE;
	}

	// ------------------------------------------------------------------------
	// Get Protocol Version Again
	// ------------------------------------------------------------------------
	getProtocolVersionFn(&protocolVersion);
	printf("Protocol version: %d\n", protocolVersion);

	return EXIT_SUCCESS;
}
