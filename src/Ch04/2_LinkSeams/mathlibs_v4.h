/******************************************************************************
 * GML 4.x
 * The Game Mathematics Library (GML) for Protocol Version 4
 * ----------------------------------------------------------------------------
 * Copyright (c) 2019-2021 Fabamaq
 *****************************************************************************/
/******************************************************************************
 * This file contains the interface documentation for the interface of the
 *bingo.so library.
 *
 *      CONCEPTS
 *  Bit Arrays
 * To conserve space, it's usual to communicate using bit arrays, which are
 *represented as int arrays. We always use the same bit ordering: the first bit
 *is the least significant bit of the first integer, followed by the second
 *least significant bit and so on.
 *
 *  *INTS (ex: PRIZEINTS)
 * Bit arrays are stored as arrays of integers. Variables with names that end
 *with 'INTS' usually refer to the number of integers used to represent the bit
 *array.
 *
 *  r_ Notation
 * Variables with names that start with 'r_' are pointers to int arrays, which
 *are used to return data from the function. The size that should be allocated
 *for each of these is written under the function definition.
 *
 *
 * additional_info
 *
 *  Int Array containing all the inputrequirements variables defined. The array
 *should be assigned as follow:
 *
 *  additional_info[0] = InputRequirements (value returned by
 *getInputRequirements() )
 *      ...
 *  additional_info[n] = InputRequirements
 *  additional_info[n+1] = InputRequirements value
 *
 * -----------------------
 *
 *****************************************************************************/

#ifndef _gml4_h_
#define _gml4_h_

#include <cstdint>

///////////////////////////////////////////////////////////////////////////////
/// GML API Tokens
///////////////////////////////////////////////////////////////////////////////

// #define GML_MAX_NUMBER_OF_GAME_MODES 4

/// @brief Defines the possible Game Types for the FMQ Portfolio of Games
struct GML_GameTypes {
	enum {
		error = -1,
		begin, // Meta Information
		Bingo = 1,
		Spins,
		Poker,
		Baccarat,
		BlackJack,
		FrenchBanking,
		FlashBingo,
		end // Meta Information
	};
};

/// @brief Defines the possible levesl for the MultiPots Bonus
struct GML_MultiPotsLevels {
	enum { MEGA, GRAND, MAJOR, MINOR, MINI, NONE };
};

/// @brief Defines the possible keys for the additionalInfo
struct GML_InputRequirementsKeys {
	enum {
		error = -1,
		game_type,		// @sa GML_GameTypes
		is_mega_active, // 0: false; 1: true
		game_mode,		// 0..GML_MAX_NUMBER_OF_GAME_MODES (used in Flash Bingo
						// Games)
		sv_multi_progressive, // @sa GML_MultiPotsLevels
		current_credits,	  // the current credits the player has

		// Meta Information
		size,
		RequirementOverflow = 32
	};
};

///////////////////////////////////////////////////////////////////////////////
/// GML API functions
///////////////////////////////////////////////////////////////////////////////

extern "C" {

/******************************************************************************
 * @brief
 *
 * @param r_is_debug
 *****************************************************************************/
void isDebug(int *r_is_debug); /*
	r_debug[0] = 1 or 0
*/

/******************************************************************************
 * Write the protocol version used by this math.
 * With this interface, the answer is, obviously, always 4.
 *****************************************************************************/
void getProtocolVersion(int *r_protocol_version); /*
	r_version[0] = PROTOCOL_VERSION
*/

/******************************************************************************
 * Returns the Input Requirements bit key that lists the required fields to
 * ... send in the getGame additional_info. @sa InputRequirementsKeys
 *****************************************************************************/
int getInputRequirements();

/******************************************************************************
 * Writes a list of properties that describe this math.
 *****************************************************************************/
void getParameters(int *r_parameters); /*
	r_parameters[0] = BALLS
	r_parameters[1] = DRAWBALLS
	r_parameters[2] = EXTRABALLS
	r_parameters[3] = CARDS
	r_parameters[4] = CARDCELLS
	r_parameters[5] = CELLROWS
	r_parameters[6] = CELLCOLUMNS
	r_parameters[7] = PRIZES
	r_parameters[8] = BALLUNIVERSEMIN
	r_parameters[9] = BALLUNIVERSEMAX
	r_parameters[10] = GENERATESINGLECARD
	r_parameters[11] = CLIENTCANSETCARDS
	r_parameters[12] = TOTALNUMBEROFSPECIALPRIZES
	r_parameters[13] = jackpotprizes[0]
	r_parameters[14] = bingoprizes[0]
	r_parameters[15] = specialprizes[0]
	r_parameters[16] = CARDINTS
	r_parameters[17] = CELLINTS
	r_parameters[18] = PRIZESINTS
	r_parameters[19] = SPECIALPRIZESINTS
	r_parameters[20] = GAMETYPE (1-BINGO; 2-SPINS)
	r_parameters[21] = 0
	r_parameters[22] = WILDBALLNUMBER
	r_parameters[23] = HASWILDBALL
	r_parameters[24] = ISDEBUG
	r_parameters[25] = RTP
	r_parameters[26] = DOBRA_MAX_REPETITIONS
	r_parameters[27] = DOBRA_MAX_VALUE
	r_parameters[28] = HIGHESTPRIZEONLY
	r_parameters[29] = NUMBER_OF_MULTIPLIER_PROFILES
	r_parameters[30] = HAS_GOLDENBALL
	r_parameters[31] = HAS_EASY_WIN
	r_parameters[32] = HAS_MEGA_BINGO
	r_parameters[33] = HAS_MEGA_BONUS
	r_parameters[34] = HAS_MEGA_SALES
	r_parameters[35] = HAS_MULTI_JACKPOT
	r_parameters[36] = ALLOWED_DENOMINATIONS
	r_parameters[37] = HAS_DISCARD
	r_parameters[38] = 0; // RESERVED FOR SUPER_WILD_BALL_NUMBER
	r_parameters[39] = 0; // RESERVED FOR HAS_SUPER_WILD_BALL
	r_parameters[40] = 0; // SPECIAL_BALLS_TYPE (enum needed for types)
	r_parameters[41] = NUMBER OF GAME MODES
	r_parameters[42] = NUMBER OF CARD MODES
	r_parameters[43] = JUMBO_LINK_NUMBER_OF_SYMBOLS
	r_parameters[44] = PRICE_PER_OPEN_CARD
	r_parameters[45] = HAS_SLOT_BINGO_BONUS
	r_parameters[46] = HAS_DYNAMIC_NUMBER_OF_EXTRA_BALLS
	r_parameters[47] = HAS_SERVER_MULTIPOTS
	r_parameters[48] = MULTIPLAYER_MATH_INDEX
	r_parameters[49] = MAX_BET
	r_parameters[50] = HAS_SET_GAME_SEED_REQUEST
*/

/******************************************************************************
 * Writes a string with the complete name of this math.
 * Example: "M-ELTORERO-0.0.4-DEBUG"
 *****************************************************************************/
void getVersion(char *r_version); /*
	r_version[64]
*/

/******************************************************************************
 * @brief Get the Allowed Denominations object
 *
 * @param r_denominations
 *****************************************************************************/
void getAllowedDenominations(int *r_denominations); /*
	r_denominations[ALLOWED_DENOMINATIONS]
*/

/******************************************************************************
 * Writes a table with the amount of credits paid by each prize, for each number
 * of open cards.
 *****************************************************************************/
void getPrizeTable(int *r_prize_table); /*
	r_prize_table[CARDS][PRIZES]
*/

/******************************************************************************
 * Writes a table with the amount of credits paid by each prize including
 * average bonus, for each number of open cards.
 *****************************************************************************/
void getPayTable(int *r_pay_table); /*
	r_pay_table[CARDS][PRIZES];
*/

/******************************************************************************
 * Writes a list that maps each prize to the prize group it belongs to.
 *****************************************************************************/
void getPrizeGroup(int *r_prize_group); /*
	r_prize_group[PRIZES]
*/

/******************************************************************************
 * Writes a list that maps each prize to the corresponding prize mask,
 * represented as a bit array.
 *****************************************************************************/
void getPrizeMasks(int *r_prize_masks); /*
	r_prize_masks[PRIZES][CELLINTS]
*/

/******************************************************************************
 * Writes 3 tables, each one contains a bit array that tells if prize N is a
 * jackpot, bingo or special prize respectively.
 *****************************************************************************/
void getPrizeInfo(int *r_jackpot_prizes, int *r_bingo_prizes,
				  int *r_special_prizes); /*
r_jackpot_prizes[PRIZESINTS];
r_bingo_prizes[PRIZESINTS];
r_special_prizes[PRIZESINTS];
*/

/******************************************************************************
 * @brief Get the Special Prize Info object
 *
 * @param r_prizes
 *****************************************************************************/
void getSpecialPrizeInfo(int *r_prizes); /*
	r_prizes[PRIZES][SPECIALPRIZEINTS]
*/

/******************************************************************************
 * @brief Get the Multiplier Profiles object
 *
 * @param r_multipliers
 *****************************************************************************/
void getMultiplierProfiles(int *r_multipliers); /*
	r_multipliers[NUMBER_OF_MULTIPLIER_PROFILES][PRIZES]
*/

/******************************************************************************
 * @brief Get the Mega Bingo Values object
 *
 * @param r_bingo_values
 * @param bet
 *****************************************************************************/
void getMegaBingoValues(int *r_bingo_values, int bet); /*
	r_bingo_values[DRAWBALLS + EXTRABALLS]
*/

/******************************************************************************
 * @brief Get the Mega Play Cost object
 *
 * @param r_cost
 *****************************************************************************/
void getMegaPlayCost(int *r_cost); /*
	r_cost[CARDS]
*/

/******************************************************************************
 * Generates random cards and writes them in a list of cards. When the game has
 * GENERATESINGLECARD, it generates a single card per call instead.
 *****************************************************************************/
void getCards(int *r_cards); /*
	cards_per_request = (GENERATESINGLECARD == 1) ?
		1 :
		CARDS;

	r_cards[cards_per_request][CARDCELLS]
*/

///////////////////////////////////////////////////////////////////////////////
/// Seeding control methods
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 * @brief Get the Card Seed object
 *
 * @param seed1
 * @param seed2
 *****************************************************************************/
void getCardSeed(long unsigned int *seed1, long unsigned int *seed2);

/******************************************************************************
 * @brief Get the Game Seed object
 *
 * @param seed1
 * @param seed2
 *****************************************************************************/
void getGameSeed(long unsigned int *seed1, long unsigned int *seed2);

/******************************************************************************
 * @brief Set the Game Seed object
 *
 * @param seed1
 * @param seed2
 *****************************************************************************/
void setGameSeed(long unsigned int seed1, long unsigned int seed2);

/******************************************************************************
 * @brief Set the Card Seed object
 *
 * @param seed1
 * @param seed2
 *****************************************************************************/
void setCardSeed(long unsigned int seed1, long unsigned int seed2);

/******************************************************************************
 * @brief
 *
 *****************************************************************************/
void rotateSeed(void);

///////////////////////////////////////////////////////////////////////////////
/// Gameplay methods
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 * @brief Generates a random play using the chosen cards and bet.
 *
 * @param output            - The buffer in where to write the play results.
 * @param current_cards     - The cards to be used during current play.
 * @param bet               - The bet (in credits) per card for the current
 * play.
 * @param denomination      - The amount of cents that correspond to 1 credit.
 * @param opencards         - The bit array of the current play's cards'
 * visibility.
 * @param eligible          - The flag indicating if the player eligible for
 * jackpot.
 * @param additional_info   - Additional information for the current play.
 * @sa getInputRequirements and @sa GML_InputRequirementsKeys
 *
 * @returns                 - Returns true when the math is initialized.
 *****************************************************************************/
bool getGame(int *output, int *current_cards, int bet, int denomination,
			 int *open_cards, bool eligible,
			 int *additional_info); /**
									 * output[4096];     // Depending on the
									 * game, a bigger size might be required.
									 * current_cards[CARDS][CARDINTS];
									 * open_cards[CARDINTS];
									 */

/******************************************************************************
 * Generates a random special kind of play, that could be many different things.
 * @param special_prize_type    - The type of special play.
 * @param bet                   - Amount of credits bet per card for this play.
 * Multiplies prizes won.
 * @param denomination          - The amount of cents that correspond to 1
 * credit.
 * @param r_aditional_info      - Used for 2 things: to pass aditional
 * information about the special prize, using the format specified below, after
 * [input], and to get the answer back, using the format after [output].
 *****************************************************************************/
void getSpecialGame(int special_prize_type, int bet, int denomination,
					int *r_aditional_info); /*
enum SpecialPrizeType { DOBRA, BONUS, BONUS_MULTIPLE, FREE_SPINS, MULTI_BONUS,
QUICK_BONUS, JACKPOT, BINGO, MULTI_JACKPOT_BONUS }; SpecialPrizeType
special_prize_type;

switch (special_prize_type) {
case DOBRA:
[input]
r_aditional_info[0]: total bet in credits (value to be doubled).

[output]
r_aditional_info[0]: data size of r_aditional_info[] (always 2).
r_aditional_info[1]: win (1) or lose (0).
r_aditional_info[2]: special game winnings.

case BONUS:
[output]
r_aditional_info[0]: data size of r_aditional_info[] (always 1).
r_aditional_info[1]: bonus value.

case BONUS_MULTIPLE:
[input]
r_aditional_info[0]: pattern of the prize.

[output]
r_aditional_info[0]: data size of r_aditional_info[]
r_aditional_info[1]: bonus value 1.
...
r_aditional_info[N]: bonus value N.

case FREE_SPINS:
// Doesn't exist for bingo games

case MULTI_BONUS:
case QUICK_BONUS:
[input]
r_aditional_info[0]: pattern of the prize.

[output]
r_aditional_info[0]: data size of r_aditional_info[] (always 1).
r_aditional_info[1]: bonus value.

case JACKPOT:
case BINGO:
// Not allowed

case MULTI_JACKPOT_BONUS:
[input]
r_aditional_info[0]: win (1) or lose (0).

[output]
r_aditional_info[0]: data size of r_aditional_info[].
r_aditional_info[1]: player picks.
r_aditional_info[2]: bonus value 1.
...
r_aditional_info[N]: bonus value N.

case SPIN_BONUS:
[output]
r_aditional_info[0]: data size of r_aditional_info[].
r_aditional_info[1]: total prize.
r_aditional_info[2]: total number of plays.
r_aditional_info[3]: number_of_columns
r_aditional_info[4]: number_of_rows
r_aditional_info[5]: has_free_spins

//PLAY 1
r_aditional_info[6, 6 + (number_of_columns * number_of_rows)]: symbols row by
row ex: number_of_columns = 4, number_of_rows = 2 [6,   6+1, 6+2, 6+3] -> row 1
[6+3, 6+4, 6+5, 6+6] -> row 2
r_aditional_info[... + 1]: prize

//if has_free_spins
r_aditional_info[... + 2]: number of extra frees pins

...

//PLAY N
r_aditional_info[N, N + (number_of_columns * number_of_rows)]: symbols row by
row r_aditional_info[... + 1]: prize
//if has_free_spins
r_aditional_info[... + 2]: number of extra frees pins

case JUMBO_LINK:
[input]
r_additional_info[0]: data size of r_additional_info[] (always 1).
r_additional_info[1]: bonus value type { 0: None , 1 : JumboBonus }.

[output]
r_additional_info[0]: data size
r_additional_info[1]: total_wins
r_additional_info[2]: total_plays

--- Data below is repeated by total number of plays ---
r_additional_info[3 -> 3 + view size]: symbol
r_additional_info[3 -> 3 + view size + 1] : winnings so far
r_additional_info[3 -> 3 + view size + 2] : play left to show to player
}
*/

/******************************************************************************
 * @brief Processes the end of a game
 *
 * @param end_state
 *****************************************************************************/
void endGameState(int *end_state); /*
	end_state[32];
	end_state[0] = number of extra balls played
	end_state[1] = cancelled_by
	cancelled_by:
		1 - Player cancelled;
		2 - Machine cancelled;
*/

/******************************************************************************
 * Only available in DEBUG maths.
 * Similar to 'getGame' above, but allows the user to force the desired balls to
 * be drawn.
 * @param balls   - List with the balls that should be drawn, in the order they
 * should be drawn.
 *****************************************************************************/
bool getGameWithBalls(int *r_output, int *current_cards, int bet,
					  int denomination, int *open_cards, bool eligible,
					  int *additional_info, int *balls); /*
   balls[DRAWBALLS + EXTRABALLS];
   */

/******************************************************************************
 * Similar to 'getGame', but is used to continue a play after the player has the
 * chance to pick one ball.
 * @param balls   - List with the balls drawn up to this point, including the
 * one that the player picked. After these, all balls should be filled with
 * WILDBALLNUMBER.
 *****************************************************************************/
bool getGameAfterWild(int *r_output, int *cards, int bet, int denomination,
					  int *open_cards, bool eligible, int *additional_info,
					  int *balls); /*
balls[DRAWBALLS + EXTRABALLS];
*/

/******************************************************************************
 * Similar to 'getGame', but is used to continue a play after the player has the
 * chance to discard some cards.
 * @param balls_played  - Number of balls already played.
 * @param balls         - List with the balls drawn up to this point.
 *****************************************************************************/
void getGameAfterDiscard(int *r_output, int *cards, int bet, int denomination,
						 int *open_cards, bool eligible, int *additional_info,
						 int balls_played, int *balls); /*
balls[balls_played];
*/

/******************************************************************************
 * Returns a new price for the extra ball after a discard card event.
 *****************************************************************************/
void refreshExtraBallCost(int *r_output, int *p_cards, int bet,
						  int denomination, int *open_cards, bool eligible,
						  int *additional_info, int balls_played, int *p_balls);

void setMultiProgressiveBaseValue(int value);

/******************************************************************************
 * Note: (a)
 * Used to cross-check game and math in-game state after game restore.
 * @params value - Value that represents the in-game state.
 *  0 - Not in Play
 *  1 - In Play
 *****************************************************************************/
void gameStateAfterRestore(int value);

///////////////////////////////////////////////////////////////////////////////
/// Bingo Bonus API
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 * Writes a list that maps each prize to the prize group it belongs to.
 *
 * @param p_prize_group - the pointer where to write the prize_group data.
 *****************************************************************************/
void getBingoBonusPrizeGroup(int *p_prize_group);

/******************************************************************************
 * Writes a list that maps each prize to the corresponding prize mask,
 * ... represented as a bit array.
 *
 * @param p_prize_mask - the pointer where to write the prize_mask data
 *****************************************************************************/
void getBingoBonusPrizeMask(int *p_prize_mask);

/******************************************************************************
 * Writes a table with the amount of credits paid by each prize,
 * ... for each number of open cards.
 *
 * @param p_prize_table - the pointer where to write the prize_table data
 *****************************************************************************/
void getBingoBonusPrizeTable(int *p_prize_table);

/******************************************************************************
 * Writes a list of properties that describe the BingoBonus math
 *
 * The p_parameters array will return the follow structure:
 * - p_parameters[ 0] = ACTUAL_BALL_UNIVERSE (range of possible balls)
 * - p_parameters[ 1] = DRAWBALLS (number_of_balls_for_start_draw)
 * - p_parameters[ 2] = 0; // reserved for EXTRABALLS
 * - p_parameters[ 3] = NUMBER_OF_CARDS
 * - p_parameters[ 4] = NUMBER_OF_CELLS
 * - p_parameters[ 5] = NUMBER_OF_ROWS (each card has)
 * - p_parameters[ 6] = NUMBER_OF_COLS (each card has)
 * - p_parameters[ 7] = PRIZES
 * - p_parameters[ 8] = BALL_UNIVERSE_MIN
 * - p_parameters[ 9] = BALL_UNIVERSE_MAX
 * - p_parameters[10] = GENERATE_SINGLE_CARD
 * - p_parameters[11] = 0; // reserved for CLIENT_CAN_SET_CARDS
 * - p_parameters[12] = 9; // TOTAL_NUMBER_OF_SPECIAL_PRIZES
 * - p_parameters[13] = JACKPOT_PRIZES_MASK
 * - p_parameters[14] = BINGO_PRIZES_MASK
 * - p_parameters[15] = SPECIAL_PRIZES_MASK
 * - p_parameters[16] = CARDS_INTS(*)
 * - p_parameters[17] = CELLS_INTS(*)
 * - p_parameters[18] = PRIZES_INTS(*)
 * - p_parameters[19] = SPECIAL_PRIZES_INTS(*)
 * - p_parameters[20] = IS HIGHEST PRIZE ONLY
 *
 * (*) Number of Integers required to store the value
 *
 * @param p_parameters - the pointer where to write the parameters data
 *****************************************************************************/
void getBingoBonusParameters(int *p_parameters);

/******************************************************************************
 * Writes an encoded list of special prizes for each prize in the prize table
 *
 * @param p_special_prize_info - the pointer where to write the prize data
 *****************************************************************************/
void getBingoBonusSpecialPrizeInfo(int *p_special_prize_info);

/******************************************************************************
 * Writes the start number of plays that are given to the player as it enters
 * the bonus as well as the number of extra plays the player wins if it hits a
 * certain prize.
 *
 * Example: [ 2, 11, 5]
 *  - 2 is the size of the information
 *  - 11 is the number of start plays
 *  - 5 is the number of extra plays won each time a certain prize is marked
 *
 * @param p_output - the pointer where to write the information
 *****************************************************************************/
void getBingoBonusNumberOfPlaysInformation(int *p_prize_table);

/******************************************************************************
 * @brief
 * - Generates a random special play for the given bet and denomination,
 * ... and writes it to the p_output array.
 * - It writes the cards data into the p_cards array
 * ... of size [NUMBER_OF_CARDS*NUMBER_OF_CELLS]
 *
 * The p_output array will return the follow structure:
 * - p_output[0]: the total size of the p_output array
 * - p_output[1]: the total wins of the special bonus
 * - p_output[2]: the total plays of the special bonus
 *
 * - From index 2 onwards, the output follows the getGame output protocol
 * ... with a trailing index for the remaining_plays for each play
 *
 * - p_output[N], N being the element at the index TOTAL_SIZE,
 * ... is a terminating flag with the hex value: 0xDEADC0DE.
 *
 * @param p_output - the pointer where to write the play data
 * @param p_cards - the pointer where to write the cards used in the bonus
 * @param bet - the bet (in credits) per card for the play
 * @param denom - the amount of cents corresponding to 1 credit
 *****************************************************************************/
void getBingoBonusSpecialGame(int *p_output, int *p_cards, int bet,
							  int denomination);

/******************************************************************************
 * @brief
 * - Function that should be used to retrieve the jumbo link symbols and their
 * definitions.
 *
 * The p_output array will return the follow structure:
 * - p_output[0]: the total size of the p_output array as N
 * - p_output[1]: The default symbol number
 *
 * ----- Start Symbol Parsing -----
 * - p_output[2]: Symbol ID
 * - p_output[3]: Symbol value
 * - p_output[4]: Symbol Type
 *  .....
 *   p_output[N-3]: Symbol ID
 *   p_output[N-2]: Symbol value
 *   p_output[N-1]: Symbol Type
 * ----- End Symbol Parsing -----
 * @param p_output - the pointer where to write the play data
 *****************************************************************************/
void getEasyLinkValues(int *p_output);

///////////////////////////////////////////////////////////////////////////////
/// Flash Bingo API
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 * @brief Writes the number of balls to draw, by game mode
 *
 * @param output - the pointer where to write the draw_sizes
 *****************************************************************************/
void getFlashBingoDrawSizes(int *output);

/******************************************************************************
 * @brief Writes the costs of the start draw, for the given number_of_cards
 *
 * @param output - the pointer where to write the draw_costs
 * @param number_of_cards
 *****************************************************************************/
void getFlashBingoDrawCostsForCardMode(int *output, int number_of_cards);

/******************************************************************************
 * @brief Writes the available card modes (layouts)
 *
 * @param output - the pointer where to write the card_modes
 *****************************************************************************/
void getFlashBingoCardModes(int *output);

///////////////////////////////////////////////////////////////////////////////
/// Mini Slots Bonus
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 * @brief It returns the size of the array that should be passed to
 *getSlotBonusParameters
 *****************************************************************************/
int getMiniSlotsBonusParametersSize();

/******************************************************************************
 * Writes a list of properties that describe the SlotBonus
 *
 * The p_parameters array will return the follow structure:
 * - p_parameters[ 0] = VERSION
 * - p_parameters[ 1] = DEFAULT NUMBER OF GAMES
 * - p_parameters[ 2] = NUMBER OF CARD ROWS
 * - p_parameters[ 3] = NUMBER OF CARD COLUMNS
 * - p_parameters[ 4] = AWARDS CREDITS (either 0 or 1)
 * - p_parameters[ 5] = AWARDS BONUS (mask)
 * - p_parameters[ 6] = NUMBER OF DIFFERENT SYMBOLS
 * - p_parameters[ 7] = NUMBER OF DIFFERENT NON BONUS SYMBOLS
 * - p_parameters[ 8] = BASE VALUE FOR REGULAR SYMBOL
 * - p_parameters[ 9] = ZERO AWARD SYMBOL VALUE
 * - p_parameters[10] = EXTRA BALL AWARD SYMBOL VALUE
 * - p_parameters[11] = MULTIPOTS AWARD SYMBOL VALUE
 * - p_parameters[12] = FREE SPINS AWARD SYMBOL VALUE
 * - p_parameters[13] = FLASH LINK AWARD SYMBOL VALUE
 *
 * (*) Number of Integers required to store the value
 *
 * @param p_parameters - the pointer where to write the parameters data
 *****************************************************************************/
void getMiniSlotsBonusParameters(int *output);

/******************************************************************************
 * @brief Copies into output parameter the prizeTable of the mini slots bonus.
 *
 *   The output array will return the following structure where X starts at 0
 *   and ends at the total number of symbols, and where Y starts at 1 (first
 * prize) and ends at the last prize the symbol has: For each symbol:
 *   - output[X] = SYMBOL
 *   For each prize in symbol
 *   - output[X + Y] = PRIZE
 *
 *   ex: (300, 5, 10, 20, 301, 10, 20, 50)
 *      (symb, p, p,  p, symb, p,  p,  p)
 *
 * @param output - The pointer to which the output will be written
 *****************************************************************************/
void getMiniSlotsBonusPrizeTable(int *output);

/******************************************************************************
 * @brief Generates a mini slots bonus play
 *
 * @note output should be able to receive up to 4096 ints
 *
 * @note Output should be structured according to MiniSlotsBonus Protocol and
 * @example:
 * Header:
 * - [DataSize, TotalWinnings, BonusAccess, TotalPlays, Views(N)*]
 * View:
 * - [PlayWinnigs, CurrentTotalWinnings, PlaysLeft, BonusAccess, Symbols(M)]
 * Symbols:
 * - [Symbols #1, ..., Symbols #N]
 *
 * @param output - the pointer where to write the mini slots bonus play data
 * @param input - the type of bonus that the player has access to
 * ... (0 - none, 1 - flashlink)
 *****************************************************************************/
void getMiniSlotsBonus(int *output, int input);

///////////////////////////////////////////////////////////////////////////////
/// Server MultiPots API
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 * @brief Writes the MultiPots data, required by the Server in the TCP command
 * MathConfig, into the given `buffer`.
 *
 * @note The given buffer must allocate 10 elements of size 4 bytes.
 *
 * @param buffer - The buffer where to write the data into.
 *****************************************************************************/
void gmlWriteTcpMathConfigDataInto(uint32_t (&buffer)[10]);

/******************************************************************************
 * @brief Writes the MultiPots data, required by the Server in the TCP command
 * PlayQuixant, into the given `buffer`.
 *
 * @note The given buffer must allocate 10 elements of size 4 bytes.
 *
 * @param buffer - The buffer where to write the data into.
 * @param openedCards - The number of opened cards
 * @param betLevel - The current betLevel
 * @param denomination - The current denomination value
 *****************************************************************************/
void gmlWriteTcpPlayQuixantDataInto(uint32_t (&buffer)[10], int openedCards,
									int betLevel, int denomination);

/******************************************************************************
 * @brief Processes the server response to the PlayQuixant TCP command
 * @param mathN - The N values in the response
 *****************************************************************************/
void gmlOnReceiveTcpPlayQuixantResponse(int *mathN);

/******************************************************************************
 * @brief Report Cashout
 *****************************************************************************/
void gmlReportCashout();

} // extern "C"

#endif /* _gml4_h_ */
