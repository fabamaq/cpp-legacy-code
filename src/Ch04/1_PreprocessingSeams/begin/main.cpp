#include "DFHLItem.h"
#include "DHLSRecord.h"

/// @todo we can use preprocessing seams to replace the calls to db_update
extern int db_update(int, struct DFHLItem *);

/// @note to do that, we add a localdefs.h with a definition for db_update

void account_update(int account_no, struct DHLSRecord *record, int activated) {
	if (activated) {
		if (record->dateStamped && record->quantity > MAX_ITEMS) {
			db_update(account_no, record->item);
		} else {
			db_update(account_no, record->backup_item);
		}
	}
	db_update(MASTER_ACCOUNT, record->item);
}
