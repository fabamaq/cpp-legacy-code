#include <string>

class Item {
  public:
	struct Price {
		int price;
		std::string asDisplayText() { return std::to_string(price); }
	};

	Price price() { return m_Price; }
	std::string name() { return m_Name; }

	Item(std::string barcode) : m_Barcode(barcode) {}

  private:
	std::string m_Barcode;
	std::string m_Name;
	Price m_Price;
};

class Display {
  public:
	virtual void showLine(std::string line) = 0;
};

class ArtR56Display : public Display {
  public:
	void showLine(std::string line) override {
		// TODO: implement production code
	}
};

class Sale {
  public:
	Sale(Display d) : m_Display(d) {}

	/**
	 * @brief Display the name of the item that was scanned, along with its
	 price on a cash register display
	 *
	 * @param barcode
	 */
	void scan(std::string barcode) {
		Item item = Item(barcode);
		std::string itemLine = item.name() + " " + item.price().asDisplayText();
		m_Display.showLine(itemLine);
	}

  private:
	Display &m_Display;
};

// #include test framework
class FakeTestFramework {
  public:
	class TestCase {};
};

class FakeDisplay : public Display {
  public:
	void showLine(std::string line) override { lastLine = line; }
	std::string getLastLine() { return lastLine; }

  private:
	std::string lastLine = "";
};

#include <stdexcept>

template <typename T> void assertEquals(const T &lhs, const T &rhs) {
	if (lhs != rhs)
		throw std::runtime_error("Assertion Failed");
}

using TestFramework = FakeTestFramework;
class SaleTest : TestFramework::TestCase {
	void testDisplayAnItem() {
		FakeDisplay display = FakeDisplay();
		Sale sale = Sale(display);

		sale.scan("1");
		assertEquals(std::string("Milk $3.99"), display.getLastLine());
	}
};
