#include <string>

class Item {
  public:
	struct Price {
		int price;
		std::string asDisplayText() { return std::to_string(price); }
	};

	Price price() { return m_Price; }
	std::string name() { return m_Name; }

	Item(std::string barcode) : m_Barcode(barcode) {}

  private:
	std::string m_Barcode;
	std::string m_Name;
	Price m_Price;
};

class Sale {
  public:
	/**
	 * @brief Display the name of the item that was scanned, along with its
	 price on a cash register display
	 *
	 * @param barcode
	 */
	void scan(std::string barcode) {
		Item item = Item(barcode);
		std::string itemLine = item.name() + " " + item.price().asDisplayText();
		// ...
	}
};

int main() { return 0; }