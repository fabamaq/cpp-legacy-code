#pragma once

#include <string>

#include "Display.hpp"

class Sale {
  public:
	Sale(Display &aDisplay) : mDisplay(aDisplay) {}

	void scan(std::string barcode);

  private:
	Display &mDisplay;
};
