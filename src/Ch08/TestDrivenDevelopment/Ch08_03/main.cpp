/**
 * @file main.cpp
 * @Pedro André Oliveira (ei06125@fe.up.pt)
 * @version 0.1
 * @date 2019-05-22
 *
 * @copyright Copyright (c) 2019
 *
 * @brief Test-Driven Development:
 * 1. Write a Failing Test Case
 * 2. Get It to Compile
 * 3. Make It Pass
 * TODO: 4. Remove Duplication
 * ... there is no duplication
 */

#include <cassert>
#include <vector>
using std::vector;

class InstrumentCalculator {
	vector<double> elements;

  public:
	void addElement(double value) { elements.push_back(value); }

	double firstMomentAbout(double point) {
		double numerator = 0.0;
		for (auto element = elements.begin(); element != elements.end();
			 ++element) {
			numerator += *element - point;
		}

		return numerator / elements.size();
	}
};

void testFirstMoment() {
	InstrumentCalculator calculator = InstrumentCalculator();
	calculator.addElement(1.0);
	calculator.addElement(2.0);

	assert(calculator.firstMomentAbout(2.0) == -0.5);
}

int main() {
	testFirstMoment();
	return 0;
}