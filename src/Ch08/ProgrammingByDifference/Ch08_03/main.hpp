#pragma once

#include <cassert>
#include <string>
#include <vector>

class InternetAddress {
  public:
	explicit InternetAddress(const std::string &s) : m_S(s) {}

  private:
	std::string m_S{""};
};

class Address {
  public:
	std::string toString() const { return "Address"; }
};

class Session {};

class Message {
  public:
	const std::vector<Address> &getFrom() const { return m_from; }

	void setFrom(const std::vector<Address> &from) { m_from = from; }

  private:
	std::vector<Address> m_from;
};

class MimeMessage {
  public:
	explicit MimeMessage(Session session) : m_Session(session) {}

	void setFrom(InternetAddress from) {}

  private:
	Session m_Session;
};

class Properties {
  public:
	void setProperty(std::string key, std::string value);
	std::string getProperty(std::string key);
};

class MessageForwarder {
  public:
	MessageForwarder(Properties p) : mProperties(p) {}

	void forwardMessage(Message message, Session session);
	void forwardMessage(Message message) {}

	std::string getDomain() { return "domain"; }

  protected:
	virtual InternetAddress getFromAddress(Message message);

  private:
	std::string getDefaultFrom() { return "getDefaultFrom"; }

	Properties mProperties;
};

inline Message makeFakeMessage() { return Message(); }
