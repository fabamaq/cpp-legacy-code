#include "main.hpp"

#include <catch2/catch.hpp>

class AccountDetailFrame : public Frame, ActionListener, WindowListener {
	TextField display = TextField(10);

  public:
	AccountDetailFrame() = default;

	String getDetailText() { return "detail"; }

	String getProjectionText() { return "projection"; }

	void actionPerformed(ActionEvent event) {
		String source = static_cast<String>(event.getActionCommand());
		if (source == "project activity") {
			DetailFrame detailDisplay = DetailFrame();
			detailDisplay.setDescription(getDetailText() + " " +
										 getProjectionText());
			detailDisplay.show();
			String accountDescription = detailDisplay.getAccountSymbol();
			accountDescription += ": ";
			// ...
			display.setText(accountDescription);
		}
	}
};

TEST_CASE("The Case of the Undetectable Side Effect", "[begin]") { ; }
