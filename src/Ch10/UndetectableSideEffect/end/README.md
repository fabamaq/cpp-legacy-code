# The Case of the Undetectable Side Effect

``` Java
// @file AccountDetailFrame.java
public class AccountDetailFrame extends Frame, implements ActionListener, WindowListener
{
    private TextField display = new TextField(10);
    private DetailFrame detailDisplay; // extracted field #1
    ...
    public AccountDetailFrame(...) { ... }

    public void actionPerformed(ActionEvent event) {
        String source = (String)event.getActionCommand();
        performCommand(source);
    }

    public void performCommand(String source) {
        if (source.equals("project activity")) {
            // extract method #1
            setDescription(getDetailText() + " " + getProjectionText());
            ...
            // extract method #2
            String accountDescription = getAccountSymbol();
            accountDescription += ": ";
            // extract method #3
            setDisplayText(accountDescription);
            ...
        }
    }

    // extracted method #1
    void setDescription(String description) {
            detailDisplay = new DetailFrame(); // extract field #1
            detailDisplay.setDescription(description);
            detailDisplay.show();
    }

    // extracted method #2
    String getAccountSymbol() {
        return detailDisplay.getAccountSymbol();
    }

    // extracted method #3
    void setDisplayText(String description) {
        display.setText(description);
    }
    ...
}
// @file AccountDetailFrameTest.java
public class TestingAccountDetailFrame extends AccountDetailFrame {
    String displayText = "";
    String accountSymbol = "";

    void setDescription(String description) {}
    String getAccountSymbol() { return accountSymbol; }
    void setDisplayText(String text) { displayText = text; }
}
public void testPerformCommand() {
    AccountDetailFrame frame = new TestingAccountDetailFrame();
    frame.accountSymbol = "SYM";
    frame.performCommand("project activity);
    assertEquals("SYM: basic account", frame.displayText);
}
```
