#include "main.hpp"

#include <catch2/catch.hpp>

class CCAImage {
  protected:
	void setSnapRegion(int x, int y, int dx, int dy);

  public:
	void snap();
};

class TestingCCAImageV1 : public CCAImage {
  public:
	void setSnapRegion(int x, int y, int dx, int dy) {
		// call the setSnapRegion of the superclass
		CCAImage::setSnapRegion(x, y, dx, dy);
	}
};

class TestingCCAImageV2 : public CCAImage {
  public:
	// Expose all CCAImage implementations of setSnapRegion
	// as part of my public interface. Delegate all calls to CCAImage.
	using CCAImage::setSnapRegion;
};

TEST_CASE("The Case of the Hidden Method", "[setSnapRegionTest][end]") {}
