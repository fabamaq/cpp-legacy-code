The Case of the "Helpful" Language Feature

``` csharp
/*
    HttpPostedFile class doesn't have a public constructor and
    HttpPostedFile class is Sealed (can't inherit from it)

    HttpFileCollection class doesn't have a public constructor and
    HttpFileCollection class is Sealed (can't inherit from it)
*/

public void IList getKSRStreams(HttpFileCollection files) {
    ArrayList list = new ArrayList();
    foreach(string name in files) {
        HttpPostedFile file = files[name];
        if (file.FileName.EndsWith(".ksr") ||
                (file.FileName.EndsWith(".txt")
                        && file.ContentLength > MIN_LEN)) {
            ...
            list.Add(file.InputStream);
        }
    }
    return list;
}
```
