#include "main.hpp"

#include <catch2/catch.hpp>

auto getKSRStreams(HttpFileCollection files) {
	auto list = ArrayList<HttpPostedFile>();
	for (const auto &entry : files) {
		auto name = entry.first;
		HttpPostedFile file = files[name];
		if (file.FileName.EndsWith(".ksr") ||
			(file.FileName.EndsWith(".txt") && file.ContentLength > MIN_LEN)) {
			list.Add(file.InputStream);
		}
	}
	return list;
}

TEST_CASE("The Case of the " Helpful " Language Feature",
		  "[setSnapRegionTest][begin]") {}
